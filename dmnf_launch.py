import numpy as np
import sys
from multiprocessing import Pool
from functools import partial
from tqdm import tqdm
import time
import python.nucleus as nuc
import python.galaxy as gal
import python.wimp as wimp
import python.neutrino as neut
import python.limit as lim
import python.plot_limit as plt_lim
from param import *

ti = time.time()
print('=== *************************************** ===')
print("          Dark Matter Neutrino Floor")
print('=== *************************************** ===')

if do_merge:
    print("Merging limits...")
    print("---------------------------------------------------------------")
    plt_lim.merge_two_limits(file1_m, file2_m)
    plt_lim.display_limit("merged_lim.npz")
    sys.exit()

if Nesc is None:
    print("Correction to the normalization of the velocity distribution...")
    print("---------------------------------------------------------------")
    Nesc = gal.integ_maxwell_dist()

Npts = 1000

print("Integrating WIMP velocity distribution...")
print("---------------------------------------------------------------")
int_maxwell = gal.int_maxwell_mod(Nesc, Npts)

print("Defining WIMP velocity range...")
print("---------------------------------------------------------------")
tab_v = np.linspace(0.0, gal_param["v_max"], Npts)

print("Defining recoil energy range...")
print("---------------------------------------------------------------")
tab_Er = np.logspace(-6, 2, 1000)

print("Fetching neutrino fluxes...")
print("---------------------------------------------------------------")
dict_fnu = neut.get_nu_flux()

print("Computing neutrino event rates...")
print("---------------------------------------------------------------")
all_dict_nu_r = neut.nu_rate(tab_A, tab_Z, dict_fnu)

print("Computing number of events due to neutrinos...")
print("---------------------------------------------------------------")
tab_nu_nb = neut.nu_number_total(Eth_keV, all_dict_nu_r)

if return_expo_lm:
    print(
        "Exposure to detect 200 8B neutrinos: "
        + str(200.0 / tab_nu_nb[5])
        + " ton.year"
    )
    sys.exit()
if return_expo_hm:
    print(
        "Exposure to detect 400 neutrinos: "
        + str(400.0 / np.sum(tab_nu_nb))
        + " ton.year"
    )
    sys.exit()

print("Defining Likelihood binning...")
print("---------------------------------------------------------------")
tab_E_bin = np.logspace(np.log10(Eth_keV), 2, Nbin + 1)

print("Computing number of events due to neutrinos per bin...")
print("---------------------------------------------------------------")
nu_nb_per_bin = neut.nu_number_per_bin(tab_E_bin, all_dict_nu_r)
nu_nb_per_bin_select = neut.select_relevant_nu(nu_nb_per_bin)

print("Computing discovery limit...")
print("---------------------------------------------------------------")

def find_lim(
    m_w, tab_Er, tab_A, int_maxwell, tab_v, tab_E_bin, nu_nb_per_bin_select, N_MC, sig_w
):
    sup_3sig = 0.0
    for k in range(N_MC):
        q0 = lim.q0_value(
            m_w,
            sig_w,
            tab_Er,
            tab_A,
            int_maxwell,
            tab_v,
            tab_E_bin,
            nu_nb_per_bin_select,
        )
        if np.sqrt(q0) > 3.0:
            sup_3sig += 1.0
    return [sig_w, (sup_3sig / N_MC)]

if SD:
    sig_w_tab = np.logspace(-45, -33, 10)
else:
    sig_w_tab = np.logspace(-50, -42, 8)

disc_limit = np.zeros(tab_m_w.size)

for i in tqdm(range(tab_m_w.size)):

    m_w = tab_m_w[i]

    pool = Pool()
    N_MC = 100
    func = partial(
        find_lim,
        m_w,
        tab_Er,
        tab_A,
        int_maxwell,
        tab_v,
        tab_E_bin,
        nu_nb_per_bin_select,
        N_MC,
    )
    res = pool.map(func, sig_w_tab)
    pool.close()
    pool.join()

    crude_sig = np.interp(0.9, np.array(res)[:, 1], np.array(res)[:, 0])

    sig_w_tab_f = np.logspace(np.log10(crude_sig) - 0.7, np.log10(crude_sig) + 0.7, 8)

    pool = Pool()
    N_MC = 1000
    func = partial(
        find_lim,
        m_w,
        tab_Er,
        tab_A,
        int_maxwell,
        tab_v,
        tab_E_bin,
        nu_nb_per_bin_select,
        N_MC,
    )
    res = pool.map(func, sig_w_tab_f)
    pool.close()
    pool.join()

    fine_sig = np.interp(0.9, np.array(res)[:, 1], np.array(res)[:, 0])

    disc_limit[i] = fine_sig

print("---------------------------------------------------------------")
print("Saving limit...")
np.savez(save_dir + file_lim, m_w=tab_m_w, lim=disc_limit)

te = time.time()
print('=== *************************************** ===')
print('               End of program')
print('          Execution time: '+'{0:.1f}'.format((te-ti)/60.)+' min')
print('=== *************************************** ===')
