import numpy as np
import astropy.constants as const
import scipy.constants
import astropy.units as u
import python.nucleus as nuc
from astropy.io import ascii
import warnings
from param import *


def dsig_dEr(A, Z, E_r, E_nu):
    """
    Computes the differential neutrino-nucleus cross section 
    as a function of the recoil energy and the neutrino energy

    Parameters
    __________
    A: the target number of nucleons
    Z: the target number of protons
    E_r: the recoil enery in keV
    E_nu: the neutrino energy in MeV

    Returns
    _______
    ds_dEr: the differential neutrino-nucleus cross section

    """

    m_n = const.u.to(u.GeV / const.c ** 2).value
    m_nuc = A * m_n
    G_F = scipy.constants.value("Fermi coupling constant")
    sin2tw = scipy.constants.value("weak mixing angle")
    Q_w = (A - Z) - (1.0 - 4.0 * sin2tw) * Z
    F2 = nuc.form_factor_SI(E_r, A)
    conv_fact = (0.197e-13) ** 2 / A * const.N_A.value

    ds_dEr = (
        G_F ** 2
        / (4.0 * np.pi)
        * Q_w ** 2
        * m_nuc
        * (1.0 - (m_nuc * E_r) / (2.0 * E_nu ** 2))
        * F2
        * conv_fact
    )  # in cm^2 keV^-1 ton^-1

    ds_dEr[ds_dEr < 0.0] = 0.0

    return ds_dEr


def get_nu_flux():
    """
    Extracts the neutrino flux as a function of
    energy for all considered neutrino sources

    Returns
    _______
    dict_fnu: a dictionary containing the energy values
    associated with the scaled fluxes for all neutrino sources

    """

    nu_list_file = "./data/NeutrinoList.txt"
    nu_list = ascii.read(nu_list_file)
    N_nu = nu_list["nu"].size

    dict_fnu = {}

    for i in range(N_nu):
        flux_file = "./data/" + nu_list["nu"][i] + "_scale.txt"
        flux_dat = ascii.read(flux_file)
        dict_fnu["e" + str(i)] = np.array(flux_dat["col1"])
        dict_fnu["f" + str(i)] = np.array(flux_dat["col2"])

    return dict_fnu


def nu_rate(tab_A, tab_Z, dict_fnu):
    """
    Computes the neutrino event rate at a 
    given recoil energy for a given target

    Parameters
    __________
    tab_A: the number of nucleons in each target
    tab_Z: the number of protons in each target
    dict_fnu: neutrino fluxes as a function of energy

    Returns
    _______
    all_dict_nu_r: a list of the dictionaries containing the neutrino 
    event rates for each neutrino source for all target nuclei

    """

    N_nu = int(len(dict_fnu.keys()) / 2)  # Energy and flux for each neutrino source
    sec_in_y = u.year.to("s")

    m_n = const.u.to(u.GeV / const.c ** 2).value

    Er_tab = np.logspace(-6, np.log10(200.0), 1000)

    all_dict_nu_r = []

    for s in range(len(tab_A)):
        A = tab_A[s]
        Z = tab_Z[s]
        m_nuc = A * m_n
        dict_nu_r = {}

        for i in range(N_nu):

            tab_rates = np.zeros(1000)

            for j, E_r in enumerate(Er_tab):

                Emin_nu = np.sqrt(m_nuc * E_r / 2.0)  # (GeV * keV)^0.5 --> MeV

                if (i == 1) | (i == 3) | (i == 4):  # pep and 7Be neutrinos (lines)
                    tab_rates[j] = (
                        sec_in_y
                        * dict_fnu["f" + str(i)][1]
                        * dsig_dEr(A, Z, E_r, np.array([dict_fnu["e" + str(i)][1]]))[0]
                    )
                else:
                    Emin_i = np.min(dict_fnu["e" + str(i)])
                    Emax_i = np.max(dict_fnu["e" + str(i)])
                    if Emin_nu <= Emax_i:
                        tab_Enu = np.logspace(np.log10(Emin_i), np.log10(Emax_i), 1000)
                        with warnings.catch_warnings():
                            warnings.simplefilter("ignore")
                            tab_Fnu_pow = np.interp(
                                np.log10(tab_Enu),
                                np.log10(dict_fnu["e" + str(i)]),
                                np.log10(dict_fnu["f" + str(i)]),
                            )
                        tab_Fnu = 10.0 ** tab_Fnu_pow
                        integ_Enu = tab_Fnu * dsig_dEr(A, Z, E_r, tab_Enu)
                        tab_rates[j] = sec_in_y * np.trapz(
                            integ_Enu[tab_Enu >= Emin_nu], tab_Enu[tab_Enu >= Emin_nu]
                        )

            dict_nu_r["e" + str(i)] = Er_tab
            dict_nu_r["r" + str(i)] = tab_rates

        all_dict_nu_r.append(dict_nu_r)

    return all_dict_nu_r


def nu_number_total(Eth, all_dict_nu_r):
    """
    Computes the number of neutrinos from a given source 
    for all target nuclei and energy threshold

    Parameters
    __________
    Eth: the energy threshold in keV
    all_dict_nu_r: a list of the dictionaries containing the 
    neutrino event rates for each neutrino source for all nuclei

    Returns
    _______
    tab_nu_nb: an array containing the expected
    number of neutrinos for each source

    """

    N_nu = int(len(all_dict_nu_r[0].keys()) / 2)

    tab_nu_nb = np.zeros(N_nu)

    for i in range(N_nu):
        N_nu_buff = 0
        for s in range(len(tab_frac)):
            dict_nu_r = all_dict_nu_r[s]
            Emax_nu = np.max(dict_nu_r["e" + str(i)][dict_nu_r["r" + str(i)] > 0.0])
            Emax = np.min([Emax_nu, 100.0])
            winteg = (Eth <= dict_nu_r["e" + str(i)]) & (
                dict_nu_r["e" + str(i)] <= Emax
            )
            N_nu_buff += tab_frac[s] * np.trapz(
                expo * dict_nu_r["r" + str(i)][winteg], dict_nu_r["e" + str(i)][winteg]
            )

        tab_nu_nb[i] = N_nu_buff

    return tab_nu_nb


def nu_number_per_bin(tab_E_bin, all_dict_nu_r):
    """
    Computes the number of neutrinos from a given source 
    for a given target nucleus in different energy bins

    Parameters
    __________
    tab_E_bin: an array with the energy bins in keV
    all_dict_nu_r: a list of the dictionaries containing the 
    neutrino event rates for each neutrino source for all nuclei

    Returns
    _______
    nu_nb_per_bin: a 2D array containing the expected
    number of neutrinos for each source in each bin

    """

    N_nu = int(len(all_dict_nu_r[0].keys()) / 2)
    nu_nb_per_bin = np.zeros((N_nu, tab_E_bin.size - 1))

    for i in range(N_nu):
        for j in range(tab_E_bin.size - 1):
            N_nu_buff = 0
            for s in range(len(tab_frac)):
                dict_nu_r = all_dict_nu_r[s]

                Emin = tab_E_bin[j]
                Emax = tab_E_bin[j + 1]

                E_tab_integ = np.logspace(np.log10(Emin), np.log10(Emax), 1000)
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    nu_r_interp_pow = np.interp(
                        np.log10(E_tab_integ),
                        np.log10(dict_nu_r["e" + str(i)]),
                        np.log10(expo * dict_nu_r["r" + str(i)]),
                    )
                nu_r_interp = 10.0 ** nu_r_interp_pow

                N_nu_buff += tab_frac[s] * np.trapz(nu_r_interp, E_tab_integ)

            nu_nb_per_bin[i, j] = N_nu_buff

    return nu_nb_per_bin


def select_relevant_nu(nu_nb_per_bin):
    """
    Select the relevant sources of neutrino to 
    perform the analysis (e.g. 17F neutrinos are negligible)

    Parameters
    __________
    nu_nb_per_bin: a 2D array containing the expected
    number of neutrinos for each source in each bin

    Returns
    _______
    nu_nb_per_bin_select: a 2D array containing the expected
    number of neutrinos for the relevant sources in each bin

    """

    nu_nb_per_bin_select = np.zeros((8, nu_nb_per_bin.shape[1]))

    DSNB_nu_nb = nu_nb_per_bin[9] + nu_nb_per_bin[10] + nu_nb_per_bin[11]

    Atm_nu_nb = (
        nu_nb_per_bin[12] + nu_nb_per_bin[13] + nu_nb_per_bin[14] + nu_nb_per_bin[15]
    )

    kept_ind = [0, 1, 2, 4, 5, 7]

    for i in range(6):
        nu_nb_per_bin_select[i, :] = nu_nb_per_bin[kept_ind[i], :]

    nu_nb_per_bin_select[6, :] = DSNB_nu_nb
    nu_nb_per_bin_select[7, :] = Atm_nu_nb

    return nu_nb_per_bin_select
