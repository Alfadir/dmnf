import numpy as np
import python.wimp as wimp
from iminuit import Minuit
from iminuit.util import make_func_code
from scipy.special import gammaln
import warnings
from param import *


def create_fake_data(w_nb_per_bin, nu_nb_per_bin_select):
    """
    Creates fake data by using a Poisson sampling of
    the expected number of WIMPs and neutrino in 
    each energy bin. The uncertainty on the neutrino
    fluxes is taken into account in this function

    Parameters
    __________
    w_nb_per_bin: the expected number of WIMPs per bin
    nu_nb_per_bin: the expected number of neutrino per bin
    for each source

    Returns
    _______
    fake_data: an array containing a realization of the 
    total number of neutrinos and WIMPs in each energy bin

    """

    N_nu = nu_nb_per_bin_select.shape[0]

    fake_data = np.zeros(w_nb_per_bin.size)

    for i in range(w_nb_per_bin.size):
        N_nu_i = 0
        for j in range(N_nu):
            expect_nb = (
                np.random.normal(1.0, uncertainties_nu_f[j])
                * nu_nb_per_bin_select[j, i]
            )
            if expect_nb < 0:
                sampled_nb = 0.0
            else:
                sampled_nb = np.random.poisson(expect_nb)
            N_nu_i += sampled_nb
        N_nu_i += np.random.poisson(w_nb_per_bin[i])
        fake_data[i] = N_nu_i

    return fake_data


class Likelihood:
    """
    Computes the binned likelihood function to be 
    maximized (see Eq. (6) of Ruppin et al. 2014)

    Parameters
    __________
    var: the scaling parameters in front of the expected
    numbers of WIMPs and neutrinos

    Returns
    _______
    l_val: the likelihood value for the given set
    of parameters

    """

    def __init__(self, fake_data, w_nb_per_bin, nu_nb_per_bin_select, sig_w):
        self.fake_data = fake_data
        self.w_nb_per_bin = w_nb_per_bin
        self.nu_nb_per_bin = nu_nb_per_bin_select.transpose()
        self.sig_w = sig_w
        self.N_nu = self.nu_nb_per_bin.shape[1]

        self.func_code = make_func_code(
            ["sig", "pp", "pep", "hep", "Be", "B", "O", "DSNB", "Atm"]
        )

    def __call__(self, *var):
        var_wimp = 10.0 ** var[0] / self.sig_w
        var_nu = var[1:]
        mu_nu_per_bin = np.matmul(self.nu_nb_per_bin, var_nu)
        mu_w_per_bin = var_wimp * self.w_nb_per_bin
        mu_tot_per_bin = mu_nu_per_bin + mu_w_per_bin
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            likelihood_per_bin = (
                self.fake_data * np.log(mu_tot_per_bin)
                - mu_tot_per_bin
                - gammaln(self.fake_data + 1)
            )

        l_val = -2.0 * np.sum(likelihood_per_bin)

        for i in range(self.N_nu):
            err_nu = uncertainties_nu_f[i]
            l_val += (
                np.log(2.0 * np.pi * err_nu ** 2) + ((var_nu[i] - 1.0) / err_nu) ** 2
            )

        return l_val


def min_likelihood_H(w_nb_per_bin, nu_nb_per_bin_select, fake_data, sig_w, fix_sig_w):

    log_sig_w = np.log10(sig_w)
    l_func = Likelihood(fake_data, w_nb_per_bin, nu_nb_per_bin_select, sig_w)

    is_valid = False
    count = 0

    while (not is_valid) & (count < 10):

        m = Minuit(
            l_func,
            sig=log_sig_w,
            pp=np.random.uniform(0, 2),
            pep=np.random.uniform(0, 2),
            hep=np.random.uniform(0, 2),
            Be=np.random.uniform(0, 2),
            B=np.random.uniform(0, 2),
            O=np.random.uniform(0, 2),
            DSNB=np.random.uniform(0, 2),
            Atm=np.random.uniform(0, 2),
            pedantic=False,
            limit_sig=(-50, -30),
            fix_sig=fix_sig_w,
        )

        m.migrad()
        is_valid = m.valid
        count += 1

    l_val = l_func.__call__(*np.array(m.args))

    return l_val


def q0_value(
    m_w, sig_w, tab_Er, tab_A, int_maxwell, tab_v, tab_E_bin, nu_nb_per_bin_select
):

    if SD:
        all_tab_R = wimp.rate_SD(tab_Er, m_w, sig_w, int_maxwell, tab_v)
    else:
        all_tab_R = wimp.rate_SI(tab_Er, m_w, sig_w, tab_A, int_maxwell, tab_v)
    w_nb_per_bin = wimp.w_number_per_bin(tab_E_bin, tab_Er, all_tab_R)

    fake_data = create_fake_data(w_nb_per_bin, nu_nb_per_bin_select)

    l_min_H1 = min_likelihood_H(
        w_nb_per_bin, nu_nb_per_bin_select, fake_data, sig_w, False
    )

    w_nb_per_bin = np.zeros(fake_data.size)

    l_min_H0 = min_likelihood_H(
        w_nb_per_bin, nu_nb_per_bin_select, fake_data, sig_w, True
    )

    q0 = l_min_H0 - l_min_H1

    if q0 < 0.0:
        return 0.0
    else:
        return q0
