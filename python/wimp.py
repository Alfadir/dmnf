import numpy as np
import astropy.constants as const
import astropy.units as u
import python.nucleus as nuc
import warnings
from param import *


def rate_SI(Er, m_w, sig_w, tab_A, int_maxwell, tab_v):
    """
    Computes the spin-independent WIMP event rate 
    at a given recoil energy for all given targets

    Parameters
    __________
    Er: the recoil enery in keV
    m_w: the WIMP mass in GeV/c^2
    sig_w: the SI Wimp/nucleon cross section in cm^2
    tab_A: the number of nucleons in each target
    int_maxwell: the integral of the Maxwell-Boltzmann 
    WIMP velocity distribution as a function of velocity
    tab_v: velocity vector associated with int_maxwell

    Returns
    _______
    all_w_rate: the WIMP event rate for each target

    """

    M = (1.0 * u.tonne).to(u.GeV / const.c ** 2).value
    T = (1.0 * u.year).to("s").value
    rho_0 = 0.3e6  # local density of DM in GeV/c^2/m^3
    m_n = const.u.to(u.GeV / const.c ** 2).value

    all_w_rate = []

    for A in tab_A:
        m_nuc = A * m_n

        mu_n = m_w * m_n / (m_w + m_n)
        mu_nuc = m_w * m_nuc / (m_w + m_nuc)

        sig_0 = (A * mu_nuc) ** 2 * sig_w * 1.0e-4 / mu_n ** 2

        F2 = nuc.form_factor_SI(Er, A)

        vmin = np.sqrt(Er * 1.0e-6 * m_nuc / (2.0 * mu_nuc ** 2)) * const.c.value

        w_rate = np.zeros(vmin.size)
        mask = vmin < gal_param["v_max"]
        interp_vmin = np.interp(vmin, tab_v, int_maxwell)
        w_rate_all = (
            M
            * T
            * (rho_0 * sig_0 / (2.0 * m_w * mu_nuc ** 2))
            * F2
            * interp_vmin
            * const.c.value ** 2
            * 1e-6
        )  #  From c^2/GeV to keV^-1
        w_rate[mask] = w_rate_all[mask]

        all_w_rate.append(w_rate)

    return all_w_rate


def rate_SD(Er, m_w, sig_w, int_maxwell, tab_v):
    """
    Computes the spin-dependent WIMP event rate 
    at a given recoil energy for all given targets

    Parameters
    __________
    Er: the recoil enery in keV
    m_w: the WIMP mass in GeV/c^2
    sig_w: the SI Wimp/nucleon cross section in cm^2
    int_maxwell: the integral of the Maxwell-Boltzmann 
    WIMP velocity distribution as a function of velocity
    tab_v: velocity vector associated with int_maxwell

    Returns
    _______
    all_w_rate: the WIMP event rate for each target

    """

    M = (1.0 * u.tonne).to(u.GeV / const.c ** 2).value
    T = (1.0 * u.year).to("s").value
    rho_0 = 0.3e6  # local density of DM in GeV/c^2/m^3
    m_n = const.u.to(u.GeV / const.c ** 2).value

    all_w_rate = []

    for i in range(len(tab_A_SD)):
        A_t = tab_A_SD[i]
        J_t = tab_J[i]
        Spn_t = tab_Spn[i]
        iso_f_t = tab_iso_f[i]

        w_rate = np.zeros(Er.size)

        if A_t[0] == 0:
            all_w_rate.append(0.0)
        else:
            for j in range(len(A_t)):
                A = A_t[j]
                J = J_t[j]
                Spn = Spn_t[j]
                iso_f = iso_f_t[j]

                m_nuc = A * m_n

                mu_n = m_w * m_n / (m_w + m_n)
                mu_nuc = m_w * m_nuc / (m_w + m_nuc)

                sig_0 = (
                    4.0
                    * (J + 1.0)
                    * (Spn * mu_nuc) ** 2
                    * sig_w
                    * 1.0e-4
                    / (3.0 * J * mu_n ** 2)
                )

                F2 = nuc.form_factor_SD(Er, A)

                vmin = (
                    np.sqrt(Er * 1.0e-6 * m_nuc / (2.0 * mu_nuc ** 2)) * const.c.value
                )

                mask = vmin < gal_param["v_max"]
                interp_vmin = np.interp(vmin, tab_v, int_maxwell)
                w_rate_all = (
                    iso_f
                    * M
                    * T
                    * (rho_0 * sig_0 / (2.0 * m_w * mu_nuc ** 2))
                    * F2
                    * interp_vmin
                    * const.c.value ** 2
                    * 1e-6
                )  #  From c^2/GeV to keV^-1
                w_rate[mask] += w_rate_all[mask]

            all_w_rate.append(w_rate)

    return all_w_rate


def Er_max(m_w, A):
    """
    Computes the maximum recoil energy that a
    WIMP of a given mass can induce on a given nucleus

    Parameters
    __________
    m_w: the WIMP mass in GeV/c^2
    A: the target number of nucleons

    Returns
    _______
    Er_m: the maximum recoil energy

    """

    m_N = A * const.u.to(u.GeV / const.c ** 2).value
    Er_m = (
        2.0
        * 1e6
        * m_w ** 2
        * m_N
        * (gal_param["v_max"] / const.c.value) ** 2
        / (m_N + m_w) ** 2
    )

    return Er_m


def w_number_per_bin(tab_E_bin, tab_Er, all_w_r):
    """
    Computes the number of WIMPs for a given 
    target nucleus in different energy bins

    Parameters
    __________
    tab_E_bin: an array with the energy bins in keV
    tab_Er: the energy table used to compute the WIMP event rate
    all_w_r: the WIMP event rate as a function of energy for each target

    Returns
    _______
    w_nb_per_bin: an array containing the 
    expected number of WIMPs 

    """

    w_nb_per_bin = np.zeros(tab_E_bin.size - 1)

    for i in range(tab_E_bin.size - 1):
        N_w_buff = 0
        for s in range(len(tab_frac)):
            w_r = all_w_r[s]

            if np.array(w_r).size == 1:
                N_w_buff += 0.0
            else:
                Emin = tab_E_bin[i]
                Emax = tab_E_bin[i + 1]

                E_tab_integ = np.logspace(np.log10(Emin), np.log10(Emax), 1000)
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    w_r_interp_pow = np.interp(
                        np.log10(E_tab_integ), np.log10(tab_Er), np.log10(expo * w_r)
                    )
                w_r_interp = 10.0 ** w_r_interp_pow

                N_w_buff += tab_frac[s] * np.trapz(w_r_interp, E_tab_integ)

        w_nb_per_bin[i] = N_w_buff

    return w_nb_per_bin
