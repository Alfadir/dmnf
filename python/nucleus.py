import numpy as np
import astropy.constants as const
import astropy.units as u


def form_factor_SI(Er, A):
    """
    Computes the spin-independent form factor for a 
    nucleus with A nucleons at a recoil energy Er

    Parameters
    __________
    Er: considered recoil energy in keV
    A: considered number of nucleons

    Returns
    _______
    F: the value of the spin-independent form factor

    """

    m_nucleon = const.u.to(u.GeV / const.c ** 2).value  # GeV/c^2
    q = np.sqrt(2 * m_nucleon * A * Er)  # MeV/c
    a = 0.52
    s = 0.9
    c = 1.23 * A ** (1.0 / 3.0) - 0.60
    rn = np.sqrt(c ** 2 + (7.0 / 3.0) * np.pi ** 2 * a ** 2 - 5.0 * s ** 2)

    hbarc = (const.hbar * const.c).to(u.MeV * u.fm).value  # MeV fm
    qrn = q * rn / hbarc
    qs = q * s / hbarc

    F = (
        3 * (np.sin(qrn) - qrn * np.cos(qrn)) / qrn ** 3 * np.exp(-1.0 * qs ** 2 / 2.0)
    ) ** 2

    return F


def form_factor_SD(Er, A):
    """
    Computes the spin-dependent form factor for a 
    nucleus with A nucleons at a recoil energy Er

    Parameters
    __________
    Er: considered recoil energy in keV
    A: considered number of nucleons

    Returns
    _______
    F: the value of the spin-dependent form factor

    """

    m_nucleon = const.u.to(u.GeV / const.c ** 2).value  # GeV/c^2
    q = np.sqrt(2 * m_nucleon * A * Er)  # MeV/c
    a = 0.52
    s = 0.9
    c = 1.23 * A ** (1.0 / 3.0) - 0.60
    rn = np.sqrt(c ** 2 + (7.0 / 3.0) * np.pi ** 2 * a ** 2 - 5.0 * s ** 2)

    hbarc = (const.hbar * const.c).to(u.MeV * u.fm).value  # MeV fm
    qrn = q * rn / hbarc

    F = (np.sin(qrn) / qrn) ** 2

    return F
