import numpy as np
from scipy import integrate
from param import *


def maxwell_dist(v_mod, l, b):
    """
    Computes the value of the Maxwell-Boltzmann WIMP 
    velocity distribution at a 3D coordinate v_vect

    Parameters
    __________
    v_mod, l, b: velocity vector in galactic coordinates (v,l,b)
    gal_param: properties of the velocity distribution in param.py

    Returns
    _______
    v_m: the value of the distribution at v_vect

    """

    v_square = (
        (v_mod * np.cos(l) * np.cos(b)) ** 2
        + (v_mod * np.sin(l) * np.cos(b) + gal_param["v_lab"]) ** 2
        + (v_mod * np.sin(b)) ** 2
    )

    v_m = np.zeros(v_square.size)
    mask = np.sqrt(v_square) < gal_param["v_esc"]
    v_m_all = (
        v_mod ** 2
        * np.cos(b)
        * np.exp(-1.0 * v_square / (2.0 * gal_param["sigma"] ** 2))
        / (2.0 * np.pi * gal_param["sigma"] ** 2) ** 1.5
    )
    v_m[mask] = v_m_all[mask]

    return v_m


def integ_maxwell_dist():
    """
    Computes the 3D integral of the Maxwell-Boltzmann 
    WIMP velocity distribution 

    Parameters
    __________
    gal_param: properties of the velocity distribution in param.py

    Returns
    _______
    integ: the integral of the distribution

    """

    Npts = 1000
    tab_v_mod = np.linspace(0.0, 1.5e6, Npts)
    tab_l = np.linspace(-np.pi, np.pi, Npts)
    tab_b = np.linspace(-np.pi / 2.0, np.pi / 2.0, Npts)

    tab_integ_b = np.zeros((Npts, Npts))

    for i in range(Npts):
        for j in range(Npts):
            tab_integ_b[i, j] = np.trapz(
                maxwell_dist(tab_v_mod[i], tab_l[j], tab_b), tab_b
            )

    tab_integ_l = np.zeros(Npts)

    for i in range(Npts):
        tab_integ_l[i] = np.trapz(tab_integ_b[i, :], tab_l)

    integ = np.trapz(tab_integ_l, tab_v_mod)

    return integ


def maxwell_mod(alpha, v, Nesc):
    """
    Computes the value of the Maxwell-Boltzmann WIMP 
    velocity distribution using modified coordinates

    Parameters
    __________
    alpha: angle between the WIMP velocity vector and the lab velocity
    v: module of the WIMP velocity vecor
    Nesc: integral computed with integ_maxwell_dist

    Returns
    _______
    v_m: the value of the distribution

    """

    v_square = (
        v ** 2 + gal_param["v_lab"] ** 2 + 2.0 * v * gal_param["v_lab"] * np.cos(alpha)
    )

    v_m = np.zeros(v_square.size)
    mask = np.sqrt(v_square) < gal_param["v_esc"]
    v_m_all = (
        2.0
        * np.pi
        * (v * np.sin(alpha) / Nesc)
        * (1.0 / (2.0 * np.pi * gal_param["sigma"] ** 2) ** 1.5)
        * np.exp(-1.0 * v_square / (2.0 * gal_param["sigma"] ** 2))
    )
    v_m[mask] = v_m_all[mask]

    return v_m


def int_maxwell_mod(Nesc, Npts):
    """
    Computes the integral of the Maxwell-Boltzmann WIMP 
    velocity distribution based on modified coordinates

    Parameters
    __________
    Nesc: integral computed with integ_maxwell_dist
    Npts: the number of values in the final interpolator

    Returns
    _______
    tab_integ_av: the value of the integral over alpha and v

    """

    tab_alpha = np.linspace(0, np.pi, Npts)
    tab_v = np.linspace(0.0, gal_param["v_max"], Npts)

    tab_integ_alpha = np.zeros(Npts)

    for i, v in enumerate(tab_v):
        tab_integ_alpha[i] = np.trapz(maxwell_mod(tab_alpha, v, Nesc), tab_alpha)

    tab_integ_av = np.zeros(Npts)

    for i in range(Npts - 1):
        tab_integ_av[i] = np.trapz(tab_integ_alpha[i:], tab_v[i:])

    return tab_integ_av
