import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import warnings
import matplotlib as mpl
from param import *

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    mpl.rcParams["text.usetex"] = True
    mpl.rcParams["text.latex.preamble"] = [r"\usepackage{amsmath}"]
    mpl.rcParams["text.latex.preamble"] = [r"\boldmath"]
    mpl.rcParams["xtick.direction"] = "in"
    mpl.rcParams["ytick.direction"] = "in"


def merge_two_limits(file1, file2):
    """
    Creates a combined discovery limit by merging
    the limits estimated in different mass ranges

    Parameters
    __________
    file1, file2: the two files in save_dir to be merged

    """

    disc_lim1 = np.load(save_dir + file1)
    m_w1 = disc_lim1["m_w"]
    lim1 = disc_lim1["lim"]

    disc_lim2 = np.load(save_dir + file2)
    m_w2 = disc_lim2["m_w"]
    lim2 = disc_lim2["lim"]

    w_inter = np.where(m_w2 <= np.max(m_w1))

    lim1_inter_pow = np.interp(np.log10(m_w2[w_inter]), np.log10(m_w1), np.log10(lim1))
    lim1_inter = 10.0 ** lim1_inter_pow

    kept_lim = np.minimum(lim1_inter, lim2[w_inter])

    merged_m_w_l = []
    merged_lim_l = []

    merged_m_w_l.append(m_w1[m_w1 < np.min(m_w2)])
    merged_lim_l.append(lim1[m_w1 < np.min(m_w2)])

    merged_m_w_l.append(m_w2[w_inter])
    merged_lim_l.append(kept_lim)

    merged_m_w_l.append(m_w2[m_w2 > np.max(m_w1)])
    merged_lim_l.append(lim2[m_w2 > np.max(m_w1)])

    merged_m_w = np.concatenate(merged_m_w_l)
    merged_lim = np.concatenate(merged_lim_l)

    np.savez(save_dir + "merged_lim.npz", m_w=merged_m_w, lim=merged_lim)


def display_limit(file_disp):
    """
    Creates a figure showing the neutrino floor
    for the considered experiment and save it
    it the save_dir folder

    Parameters
    __________
    file_disp: the file containing the limit
    to be displayed

    """

    disc_lim = np.load(save_dir + file_disp)

    file_save = save_dir + "nu_floor.pdf"
    with PdfPages(file_save) as pdf:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            fig, ax = plt.subplots(nrows=1, figsize=(8.0, 5.5))
            ax.set_xscale("log")
            ax.set_yscale("log")
            ax.xaxis.set_tick_params(width=1.5)
            ax.yaxis.set_tick_params(width=1.5)
            ax.xaxis.set_tick_params(which=u"minor", width=1.5)
            ax.yaxis.set_tick_params(which=u"minor", width=1.5)
            plt.rc("text", usetex=True)
            plt.rcParams["text.latex.preamble"] = [r"\boldmath"]
            plt.xlabel(r"$\mathrm{WIMP~mass~[GeV/c^2]}$", fontsize=19)
            plt.ylabel(r"$\mathrm{WIMP-nucleon~cross~section~[cm^2]}$", fontsize=19)
            plt.xticks(fontsize=17, weight=10)
            plt.yticks(fontsize=17, weight=10)
            plt.xlim([1e-2, 1e3])
            plt.ylim([1e-50, 1e-37])
            plt.grid(True, alpha=0.5, which="both")

            plt.plot(disc_lim["m_w"], disc_lim["lim"], lw=2, color="#7EA4C4")

            pdf.savefig()
            plt.close()
