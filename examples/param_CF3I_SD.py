import numpy as np

# =============
# DM Experiment
# =============
tab_A = [12.01,19.0,127.0]
tab_Z = [6.0,9.0,53.0]
tab_atom = [1.0,3.0,1.0]
Eth_keV = 1e-6  # energy threshold in keV
expo = 0.34  # exposure in ton.year

return_expo_lm = False
return_expo_hm = False

# =========================
# Spin-dependent parameters
# =========================
SD = True  # use spin-dependent interaction instead of spin-independent
tab_A_SD = [[0.],[19.],[127.0]]
tab_Z_SD = [[0.],[9.],[53.0]]
tab_J = [[0.],[0.5],[2.5]]
tab_Spn = [[0.],[0.477],[0.309]]  # mean spin content on proton
# tab_Spn = [[0.],[-0.004],[0.075]]   # mean spin content on neutron
tab_iso_f = [[0.],[1.0],[1.0]]

# ==================
# WIMP mass coverage
# ==================
tab_m_w = np.logspace(np.log10(0.01), np.log10(20.0), 10)
# tab_m_w = np.concatenate((np.logspace(np.log10(6.),np.log10(200.),9),np.array([1000.])))

# ==========
# Likelihood
# ==========
Nbin = 1  # number of energy bins in likelihood

# ============
# Save results
# ============
save_dir = "./"
file_lim = "limit_low_mass.npz"
do_merge = False
file1_m = "limit_low_mass.npz"
file2_m = "limit_high_mass.npz"

# =====================
# Velocity distribution
# =====================
gal_param = {
    "v_0": 220000.0,  # local circular velocity in m/s
    "sigma": 220000.0
    / np.sqrt(2.0),  # dispersion of the Maxwell-Boltzmann distribution
    "v_lab": 232000.0,  # lab velocity in m/s
    "v_esc": 544000.0,  # escape velocity in m/s
    "v_max": 232000.0 + 544000.0,  # maximum WIMP velocity in m/s
}
Nesc = 0.9933606016341882

# ================================
# Uncertainties on neutrino fluxes
# ================================
name_nu = ["pp", "pep", "hep", "7Be", "8B", "15O", "DSNB", "Atm"]
uncertainties_nu_f = [0.01, 0.03, 0.16, 0.10, 0.16, 0.31, 0.50, 0.20]

# =======================================
# Mass fraction of each element in target
# =======================================
tab_frac = [
    (np.array(tab_A) * np.array(tab_atom))[i]
    / np.sum(np.array(tab_A) * np.array(tab_atom))
    for i in range(len(tab_A))
]
