dmnf
===

**The dark matter neutrino floor calculator.**

[![](https://img.shields.io/badge/python-3.*-blue)](https://www.python.org/download/releases/3.0/) [![](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

``dmnf`` is a python tool that enables computing the dark matter neutrino floor associated with any direct detection experiment. This floor is a saturation regime of the discovery potential induced by the systematics associated with the fluxes of the different components in the neutrino background.

Features
--------
* Calculate the solar, diffuse supernovae, and atmospheric neutrino event rates.
* Calculate the WIMP event rates considering spin-independent or spin-dependent interations.
* Calculate the number of WIMP and neutrino events expected for a given experiment.
* Estimate the neutrino floor associated with a given experiment.
* The experiment can be based on a single nucleus (e.g. Ge) or an association of nuclei (e.g. CaWO4).
* User friendly architecture, the user just needs to provide information in a parameter file.

Required python packages
------------------------
* multiprocessing
* functools
* tqdm
* iminuit
* astropy
* matplotlib

All these packages can be installed using the pip package-management system.

Quickstart
----------
Once you have ``dmnf`` installed, you can quickly generate the neutrino floor of any dark matter experiment
by changing the content of the default parameter file param.py. Examples of parameter files for different dark matter experiments are given in the `examples` folder.
You can then launch the code from the command line:
```
python3 dmnf_launch.py
```
Attribution
-----------
Please cite [Ruppin and Billard (2021)](https://arxiv.org) and/or [Ruppin et al. (2014)](https://arxiv.org/pdf/1408.3581.pdf) (whichever is more appropriate) if you find this code useful in your research. Please also consider starring the GitLab repository.
